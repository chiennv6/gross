import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppDefaultTextField extends StatefulWidget {
  const AppDefaultTextField(
      {Key? key, required this.title, required this.onChange, this.isPassword})
      : super(key: key);

  final String title;
  final bool? isPassword;
  final dynamic onChange;

  @override
  State<AppDefaultTextField> createState() => _AppDefaultTextFieldState();
}

class _AppDefaultTextFieldState extends State<AppDefaultTextField> {
  final myController = TextEditingController();

  void _printLatestValue() {
    debugPrint(myController.text);
  }

  @override
  void initState() {
    super.initState();
    myController.addListener(_printLatestValue);
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              widget.title,
              style: GoogleFonts.inter(
                  fontSize: 12, color: const Color.fromRGBO(102, 102, 128, 1)),
            ),
            const Spacer()
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8),
          child: SizedBox(
            height: 48,
            child: TextField(
              obscureText: widget.isPassword ?? false,
              controller: myController,
              onChanged: widget.onChange,
              style: GoogleFonts.inter(fontSize: 14, color: Colors.white),
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(16),
                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(16),
                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
