import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppDefaultButton extends StatefulWidget {
  const AppDefaultButton({
    Key? key,
    required this.title,
    required this.isIcon,
    required this.icon,
    required this.bgColor,
    this.onPress,
    required this.textColor,
  }) : super(key: key);

  final String title;
  final bool isIcon;
  final String icon;
  final Color bgColor;
  final Color textColor;
  final VoidCallback? onPress;

  @override
  State<AppDefaultButton> createState() => _AppDefaultButtonState();
}

class _AppDefaultButtonState extends State<AppDefaultButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            backgroundColor: widget.bgColor,
            shape: const StadiumBorder(),
            side: const BorderSide(
                color: Color.fromRGBO(255, 255, 255, 0.15), width: 1)),
        onPressed: () {
          widget.onPress?.call();
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 14, bottom: 14),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (widget.isIcon)
                Row(
                  children: [
                    Image(
                      width: 16,
                      height: 16,
                      image: AssetImage(widget.icon),
                      fit: BoxFit.fill,
                    ),
                    const SizedBox(width: 8),
                    Text(
                      widget.title,
                      style: GoogleFonts.inter(
                          fontSize: 14,
                          color: widget.textColor,
                          fontWeight: FontWeight.w700),
                    )
                  ],
                )
              else
                Text(
                  widget.title,
                  style: GoogleFonts.inter(
                      fontSize: 14,
                      color: widget.textColor,
                      fontWeight: FontWeight.w700),
                )
            ],
          ),
        ),
      ),
    );
  }
}
