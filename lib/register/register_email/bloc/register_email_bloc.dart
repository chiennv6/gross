import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gross/register/register_email/bloc/register_email_event.dart';
import 'package:gross/register/register_email/bloc/register_email_state.dart';

class RegisterEmailBloc extends Bloc<RegisterEmailEvent, RegisterEmailState> {
  RegisterEmailBloc() : super(const RegisterEmailState()) {
    on<EmailChangeEvent>(_userChangeEvent);
    on<PassChangeEvent>(_passWordChangeEvent);
  }

  void _userChangeEvent(
      EmailChangeEvent event, Emitter<RegisterEmailState> emit) {
    emit(state.copyWith(user: event.email));
  }

  void _passWordChangeEvent(
      PassChangeEvent event, Emitter<RegisterEmailState> emit) {
    emit(state.copyWith(passWord: event.password));
  }
}
