import 'package:equatable/equatable.dart';

abstract class RegisterEmailEvent extends Equatable {
  const RegisterEmailEvent();

  @override
  List<Object> get props => [];
}

class EmailChangeEvent extends RegisterEmailEvent {
  const EmailChangeEvent({required this.email});

  final String email;

  @override
  List<Object> get props => [
        email,
      ];
}

class PassChangeEvent extends RegisterEmailEvent {
  const PassChangeEvent({required this.password});

  final String password;

  @override
  List<Object> get props => [password];
}


