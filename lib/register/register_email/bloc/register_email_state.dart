import 'package:equatable/equatable.dart';

class RegisterEmailState extends Equatable {
  const RegisterEmailState(
      {this.user = "", this.passWord = ""});

  final String user;
  final String passWord;

  RegisterEmailState copyWith(
      {String? user, String? passWord, int? strongPassword}) {
    return RegisterEmailState(
        user: user ?? this.user,
        passWord: passWord ?? this.passWord);
  }

  @override
  // TODO: implement props
  List<Object?> get props => [user, passWord];
}
