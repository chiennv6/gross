import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_password_strength/flutter_password_strength.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gross/common/app_images.dart';
import 'package:gross/custome_widget/app_default_button.dart';
import 'package:gross/custome_widget/app_default_textfield.dart';
import 'package:gross/register/register_email/bloc/register_email_bloc.dart';
import 'package:gross/register/register_email/bloc/register_email_event.dart';
import 'package:gross/register/register_email/bloc/register_email_state.dart';

class RegisterEmail extends StatelessWidget {
  const RegisterEmail({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterEmailBloc(),
      child: const RegisterEmailScreen(),
    );
  }
}

class RegisterEmailScreen extends StatefulWidget {
  const RegisterEmailScreen({super.key});

  @override
  State<RegisterEmailScreen> createState() => _RegisterEmailState();
}

class _RegisterEmailState extends State<RegisterEmailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<RegisterEmailBloc, RegisterEmailState>(
        listener: (context, state) {},
        builder: (context, state) {
          return Container(
            decoration:
                const BoxDecoration(color: Color.fromRGBO(28, 28, 35, 1)),
            child: Padding(
              padding: const EdgeInsets.only(left: 24, right: 24, bottom: 38),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 60),
                    child: Row(
                      children: const [
                        Spacer(),
                        Center(
                            child: Image(
                          image: AssetImage(AppImages.icLogo),
                          width: 178,
                          height: 29,
                        )),
                        Spacer()
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      AppDefaultTextField(
                        title: "E-mail address",
                        onChange: (value) {
                          BlocProvider.of<RegisterEmailBloc>(context)
                              .add(EmailChangeEvent(email: value));
                        },
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      AppDefaultTextField(
                          isPassword: true,
                          title: "Password",
                          onChange: (value) {
                            BlocProvider.of<RegisterEmailBloc>(context)
                                .add(PassChangeEvent(password: value));
                          }),
                      Padding(
                        padding: const EdgeInsets.only(top: 24),
                        child: FlutterPasswordStrength(
                            password: state.passWord,
                            strengthCallback: (strength) {
                              debugPrint(strength.toString());
                            }),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text(
                          "Use 8 or more characters with a mix of letters, numbers & symbols.",
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              color: const Color.fromRGBO(102, 102, 128, 1)),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 40),
                        child: AppDefaultButton(
                            title: "Get started, it’s free!",
                            isIcon: false,
                            icon: "",
                            bgColor: Color.fromRGBO(255, 121, 102, 1),
                            textColor: Colors.white),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Center(
                        child: Text(
                          "Do you have already an account?",
                          style: GoogleFonts.inter(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: AppDefaultButton(
                            onPress: () {},
                            title: "Sign In",
                            isIcon: false,
                            icon: "",
                            bgColor: const Color.fromRGBO(255, 121, 102, 1),
                            textColor: Colors.white),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
