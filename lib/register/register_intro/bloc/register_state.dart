import 'package:equatable/equatable.dart';

class RegisterState extends Equatable {
  const RegisterState(
      {this.isApple = false,
      this.isGoogle = false,
      this.isFB = false,
      this.isEmail = false});

  final bool isApple;
  final bool isGoogle;
  final bool isFB;
  final bool isEmail;

  RegisterState copyWith(
      {bool? isApple, bool? isGoogle, bool? isFB, bool? isEmail}) {
    return RegisterState(
        isApple: isApple ?? this.isApple,
        isGoogle: isGoogle ?? this.isGoogle,
        isFB: isFB ?? this.isFB,
        isEmail: isEmail ?? this.isEmail);
  }

  @override
  // TODO: implement props
  List<Object?> get props => [isApple, isGoogle, isFB, isEmail];
}
