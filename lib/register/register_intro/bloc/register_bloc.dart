import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gross/register/register_intro/bloc/register_event.dart';
import 'package:gross/register/register_intro/bloc/register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc() : super(const RegisterState()) {
    on<SignUpAppleEvent>(_signUpAppleEvent);
    on<SignUpGoogleEvent>(_signUpGoogleEvent);
    on<SignUpFaceBookEvent>(_signUpFaceBookEvent);
    on<SignUpEmailEvent>(_signUpEmailEvent);
  }

  void _signUpAppleEvent(SignUpAppleEvent event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
        isApple: true, isEmail: false, isFB: false, isGoogle: false));
  }

  void _signUpGoogleEvent(
      SignUpGoogleEvent event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
        isApple: false, isEmail: false, isFB: false, isGoogle: true));
  }

  void _signUpFaceBookEvent(
      SignUpFaceBookEvent event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
        isApple: false, isEmail: false, isFB: true, isGoogle: false));
  }

  void _signUpEmailEvent(SignUpEmailEvent event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
        isApple: false, isEmail: true, isFB: false, isGoogle: false));
  }
}
