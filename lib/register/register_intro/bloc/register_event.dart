import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable{
  const RegisterEvent();

  @override
  List<Object?> get props => [];
}

class SignUpAppleEvent extends RegisterEvent {
  const SignUpAppleEvent();

  @override
  List<Object?> get props => [];
}
class SignUpGoogleEvent extends RegisterEvent {
  const SignUpGoogleEvent();

  @override
  List<Object?> get props => [];
}
class SignUpFaceBookEvent extends RegisterEvent {
  const SignUpFaceBookEvent();

  @override
  List<Object?> get props => [];
}
class SignUpEmailEvent extends RegisterEvent {
  const SignUpEmailEvent();

  @override
  List<Object?> get props => [];
}
