import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gross/common/app_images.dart';
import 'package:gross/custome_widget/app_default_button.dart';
import 'package:gross/register/register_email/register_email.dart';
import 'package:gross/register/register_intro/bloc/register_bloc.dart';
import 'package:gross/register/register_intro/bloc/register_event.dart';
import 'package:gross/register/register_intro/bloc/register_state.dart';

class Register extends StatelessWidget {
  const Register({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterBloc(),
      child: const RegisterScreen(),
    );
  }
}

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterState();
}

class _RegisterState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<RegisterBloc, RegisterState>(
        listener: (context, state) {},
        builder: (context, state) {
          return Container(
            decoration:
                const BoxDecoration(color: Color.fromRGBO(28, 28, 35, 1)),
            child: Padding(
              padding: const EdgeInsets.only(left: 24, right: 24, bottom: 38),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 60),
                    child: Row(
                      children: const [
                        Spacer(),
                        Center(
                            child: Image(
                          image: AssetImage(AppImages.icLogo),
                          width: 178,
                          height: 29,
                        )),
                        Spacer()
                      ],
                    ),
                  ),
                  const Spacer(),
                  AppDefaultButton(
                      onPress: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const RegisterEmail()));
                      },
                      title: "Sign up with Apple",
                      isIcon: true,
                      icon: AppImages.icApple,
                      bgColor: Colors.black,
                      textColor: Colors.white),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: AppDefaultButton(
                        onPress: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const RegisterEmail()));
                        },
                        title: "Sign up with Google",
                        isIcon: true,
                        icon: AppImages.icGoogle,
                        bgColor: Colors.white,
                        textColor: Colors.black),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: AppDefaultButton(
                      onPress: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const RegisterEmail()));
                      },
                      title: "Sign up with Facebook",
                      isIcon: true,
                      icon: AppImages.icFacebook,
                      bgColor: const Color.fromRGBO(23, 113, 230, 1),
                      textColor: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 40, bottom: 40),
                    child: Text(
                      "or",
                      style: GoogleFonts.inter(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  AppDefaultButton(
                    onPress: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const RegisterEmail()));
                    },
                    title: "Sign up with E-mail",
                    isIcon: false,
                    icon: AppImages.icApple,
                    bgColor: const Color.fromRGBO(255, 255, 255, 0.1),
                    textColor: Colors.white,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 24),
                    child: Text(
                      "By registering, you agree to our Terms of Use. Learn how we collect, use and share your data.",
                      style: GoogleFonts.inter(
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                          color: const Color.fromRGBO(102, 102, 128, 1)),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
