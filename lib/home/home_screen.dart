import 'package:flutter/material.dart';
import 'package:gross/common/app_images.dart';
import 'package:gross/custome_widget/app_navigation_bottom_shape.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    pageController.addListener(() {
      setBottomBarIndex(pageController.page!.toInt());
    });
  }

  setBottomBarIndex(index) {
    setState(() {
      currentIndex = index;
    });
  }

  void switchPage(index) {
    pageController.animateToPage(index,
        duration: const Duration(microseconds: 500), curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: PageView(
              controller: pageController,
              scrollDirection: Axis.horizontal,
              children: [
                Container(
                  color: Colors.orange,
                  height: size.height,
                  width: size.width,
                  child: ListView(
                    children: [
                      Container(
                        color: Colors.green,
                        height: 300,
                      )
                    ],
                  ),
                ),
                Container(
                  color: Colors.red,
                  height: size.height,
                  width: size.width,
                ),
                Container(
                  color: Colors.blue,
                  height: size.height,
                  width: size.width,
                ),
                Container(
                  color: Colors.yellow,
                  height: size.height,
                  width: size.width,
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: SizedBox(
              width: size.width,
              height: 80,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 16, left: 24, right: 24),
                child: Stack(
                  children: [
                    CustomPaint(
                      size: Size(size.width, 100),
                      painter: AppNavigationBottomShape(),
                    ),
                    Center(
                      heightFactor: 0.6,
                      child: SizedBox(
                          width: 60,
                          height: 60,
                          child: Image.asset(
                            AppImages.icAdd,
                            fit: BoxFit.contain,
                          )),
                    ),
                    SizedBox(
                      width: size.width,
                      height: 80,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          InkWell(
                            onTap: () {
                              setBottomBarIndex(0);
                              switchPage(0);
                            },
                            child: SizedBox(
                              width: (size.width - 65 - 48) / 4,
                              height: 80,
                              child: Image.asset(currentIndex == 0
                                  ? AppImages.icHomeSelected
                                  : AppImages.icHomeUnSelected),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              setBottomBarIndex(1);
                              switchPage(1);
                            },
                            child: SizedBox(
                              width: (size.width - 65 - 48) / 4,
                              height: 80,
                              child: Image.asset(currentIndex == 1
                                  ? AppImages.icBudgetsSelected
                                  : AppImages.icBudgetsUnSelected),
                            ),
                          ),
                          const SizedBox(
                            width: 65,
                          ),
                          InkWell(
                            onTap: () {
                              setBottomBarIndex(2);
                              switchPage(2);
                            },
                            child: SizedBox(
                              width: (size.width - 65 - 48) / 4,
                              height: 80,
                              child: Image.asset(currentIndex == 2
                                  ? AppImages.icCalendarSelected
                                  : AppImages.icCalendarUnSelected),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              setBottomBarIndex(3);
                              switchPage(3);
                            },
                            child: SizedBox(
                              width: (size.width - 65 - 48) / 4,
                              height: 80,
                              child: Image.asset(currentIndex == 3
                                  ? AppImages.icCreditCardsSelected
                                  : AppImages.icCreditCardsUnSelected),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
