import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gross/common/app_images.dart';
import 'package:gross/custome_widget/app_default_button.dart';
import 'package:gross/login/login_screen.dart';
import 'package:gross/register/register_intro/register_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.bgDefault), fit: BoxFit.fill)),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 60),
            child: Row(
              children: const [
                Spacer(),
                Center(
                    child: Image(
                  image: AssetImage(AppImages.icLogo),
                  width: 178,
                  height: 29,
                )),
                Spacer()
              ],
            ),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 42, right: 43),
            child: Center(
              child: Text(
                "Congue malesuada in ac justo, a tristique leo massa. Arcu leo leo urna risus.",
                textAlign: TextAlign.center,
                style: GoogleFonts.inter(
                  color: Colors.white,
                  fontSize: 14,
                  textStyle: Theme.of(context).textTheme.displayMedium,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 26, right: 26, top: 40),
            child: AppDefaultButton(
              onPress: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Register()));
              },
              title: 'Get started',
              isIcon: false,
              icon: '',
              bgColor: const Color.fromRGBO(255, 121, 102, 1),
              textColor: Colors.white,
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 26, right: 26, top: 16, bottom: 30),
            child: AppDefaultButton(
                onPress: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => const Login()));
                },
                title: "I have an account",
                isIcon: false,
                icon: "",
                bgColor: const Color.fromRGBO(255, 255, 255, 0.15),
                textColor: Colors.white),
          ),
        ],
      ),
    );
  }
}
