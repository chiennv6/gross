class AppImages {
  AppImages._();

  static const String icLogo = 'resources/icons/ic_logo.png';
  static const String icApple = 'resources/icons/apple.png';
  static const String icFacebook = 'resources/icons/facebook.png';
  static const String icGoogle = 'resources/icons/google.png';
  static const String icAdd = 'resources/icons/icon_add.png';

  static const String icHomeSelected = 'resources/icons/ic_home_selected.png';
  static const String icCreditCardsSelected =
      'resources/icons/ic_credit_cards_selected.png';
  static const String icCalendarSelected =
      'resources/icons/ic_calendar_selected.png';
  static const String icBudgetsSelected =
      'resources/icons/ic_budgets_selected.png';

  static const String icHomeUnSelected = 'resources/icons/ic_home_unselect.png';
  static const String icCreditCardsUnSelected =
      'resources/icons/ic_credit_cards_unselect.png';
  static const String icCalendarUnSelected =
      'resources/icons/ic_calendar_unselect.png';
  static const String icBudgetsUnSelected =
      'resources/icons/ic_budgets_unselect.png';

  //MARK: Images
  static const String bgDefault = 'resources/images/wellcome_screen.png';
  static const String linearGradientBtn =
      'resources/images/linear_gradien_button.png';
  static const String bgNavigationBottom = 'resources/images/bg.png';
}
