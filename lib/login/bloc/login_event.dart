import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object?> get props => [];
}

class UserChangeEvent extends LoginEvent {
  const UserChangeEvent({required this.user});

  final String user;

  @override
  List<Object?> get props => [user];
}

class PassWordChangeEvent extends LoginEvent {
  const PassWordChangeEvent({required this.password});

  final String password;

  @override
  List<Object?> get props => [];
}

class SignInEvent extends LoginEvent {
  const SignInEvent();

  @override
  List<Object?> get props => [];
}
