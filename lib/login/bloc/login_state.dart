import 'package:equatable/equatable.dart';

class LoginState extends Equatable {
  const LoginState(
      {this.user = "",
      this.passWord = "",
      this.loginSuccess = false});

  final String user;
  final String passWord;
  final bool loginSuccess;

  LoginState copyWith(
      {String? user,
      String? passWord,
      bool? loginSuccess}) {
    return LoginState(
        user: user ?? this.user,
        passWord: passWord ?? this.passWord,
        loginSuccess: loginSuccess ?? this.loginSuccess);
  }

  @override
  // TODO: implement props
  List<Object?> get props => [user, passWord, loginSuccess];
}
