import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gross/login/bloc/login_event.dart';
import 'package:gross/login/bloc/login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(const LoginState()) {
    on<UserChangeEvent>(_userChangeEvent);
    on<PassWordChangeEvent>(_passWordChangeEvent);
    on<SignInEvent>(_signEvent);
  }

  void _userChangeEvent(UserChangeEvent event, Emitter<LoginState> emit) {
    emit(state.copyWith(user: event.user));
  }

  void _passWordChangeEvent(
      PassWordChangeEvent event, Emitter<LoginState> emit) {
    emit(state.copyWith(passWord: event.password));
  }

  void _signEvent(SignInEvent event, Emitter<LoginState> emit) {
    if (state.user.isEmpty || state.passWord.isEmpty) {
      emit(state.copyWith(loginSuccess: false));
    } else {
      emit(state.copyWith(loginSuccess: true));
    }
  }
}
