import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gross/common/app_images.dart';
import 'package:gross/custome_widget/app_default_button.dart';
import 'package:gross/custome_widget/app_default_textfield.dart';
import 'package:gross/home/home_screen.dart';
import 'package:gross/login/bloc/login_bloc.dart';
import 'package:gross/login/bloc/login_event.dart';
import 'package:gross/login/bloc/login_state.dart';

class Login extends StatelessWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginBloc(),
      child: _Login(),
    );
  }
}

class _Login extends StatefulWidget {
  @override
  State<_Login> createState() => _LoginState();
}

class _LoginState extends State<_Login> {
  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.grey;
    }
    return Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<LoginBloc, LoginState>(
        listener: (context, state) {
          if (state.loginSuccess) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const HomeScreen()),
                (Route<dynamic> route) => false);
          }
        },
        builder: (context, state) {
          return Container(
            decoration:
                const BoxDecoration(color: Color.fromRGBO(28, 28, 35, 1)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 60),
                  child: Row(
                    children: const [
                      Spacer(),
                      Center(
                          child: Image(
                        image: AssetImage(AppImages.icLogo),
                        width: 178,
                        height: 29,
                      )),
                      Spacer()
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 24, right: 24),
                  child: Column(
                    children: [
                      AppDefaultTextField(
                          title: "Login",
                          onChange: (value) {
                            BlocProvider.of<LoginBloc>(context)
                                .add(UserChangeEvent(user: value));
                          }),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: AppDefaultTextField(
                            title: "Password",
                            onChange: (value) {
                              BlocProvider.of<LoginBloc>(context)
                                  .add(PassWordChangeEvent(password: value));
                            }),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Checkbox(
                                    value: false,
                                    checkColor: Colors.white,
                                    fillColor:
                                        MaterialStateProperty.resolveWith(
                                            getColor),
                                    onChanged: (bool? value) {}),
                                Text(
                                  "Remember Me",
                                  style: GoogleFonts.inter(
                                      fontSize: 12,
                                      color: const Color.fromRGBO(
                                          102, 102, 128, 1)),
                                ),
                              ],
                            ),
                            Text(
                              "Forgot password",
                              style: GoogleFonts.inter(
                                  fontSize: 12,
                                  color:
                                      const Color.fromRGBO(102, 102, 128, 1)),
                            ),
                          ],
                        ),
                      ),
                      AppDefaultButton(
                          onPress: () {
                            BlocProvider.of<LoginBloc>(context)
                                .add(const SignInEvent());
                            if (state.passWord == "" || state.user == "") {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text("Không để trống")));
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text("Login thành công")));
                            }
                          },
                          title: "Sign In",
                          isIcon: false,
                          icon: "",
                          bgColor: const Color.fromRGBO(255, 121, 102, 1),
                          textColor: Colors.white),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 26, right: 26, top: 16, bottom: 30),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Text(
                          "If you don't have an account yet?",
                          style: GoogleFonts.inter(
                              fontSize: 14, color: Colors.white),
                        ),
                      ),
                      AppDefaultButton(
                          onPress: () {},
                          title: "Sign Up",
                          isIcon: false,
                          icon: "",
                          bgColor: const Color.fromRGBO(255, 255, 255, 0.1),
                          textColor: Colors.white)
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
